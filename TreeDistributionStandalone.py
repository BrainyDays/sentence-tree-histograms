import json
import pandas as pd
import plotly.express as px
import plotly as py
import sys
import time
import dash
import dash_core_components as dcc
import dash_html_components as html

def TreeLevels(in_index, out_index):
    """ Add level information to tree edges. Levels refer to outgoing node level, i.e. root is 0."""
    # Remove self edges
    ni = [in_index[j] for j in range(len(out_index)) if in_index[j]!=out_index[j]]
    ind = [j for j in range(len(out_index)) if in_index[j]==out_index[j]]
    # Initialize levels
    levels = [-2]*len(out_index)
    for i in ind:
        levels[i]=-1
    # print(f"ind={ind}\n, levels={levels}\n\n")
    # Identify root
    roo = [out_index[i] for i in range(len(out_index)) if out_index[i] not in ni]
    ind = [n for n in range(len(out_index)) if out_index[n] not in ni and out_index[n]!=in_index[n]]
    for i in ind:
        levels[i]=0
    roi = [in_index[i] for i in ind]
    # print(f"in_index={in_index}\nout_index={out_index}\nni={ni}\nroo={roo}\nroi={roi}\nind={ind}\nlevels={levels}\n\n")
    # return
    nn = 0
    while -2 in levels and nn<1000:
        nn+=1
        roo = [n for n in out_index if n in roi]
        ind = [n for n in range(len(out_index)) if out_index[n] in roi]
        for i in ind:
            if levels[i]!=-2:
                print(f"Error! level already assigned! levels[{i}]={levels[i]}, levels={levels}")
            else:
                levels[i]=nn
        roi = [in_index[i] for i in ind]
    return levels

def Edges(tree):
    """ returns all edges, pos, link type and level. Important: Each element in the input variable must have key 'levels' (if it doesn't, run function TreeLevels first) """
    # Single edges
    pmid = []
    edges = []
    edges_pos = []
    edges_tok = []
    edges_lem = []
    for tr in tree:
        if len(tr['levels']):
            e = [(tr['in_index'][i], i, tr['out_index'][i], tr['levels'][i]) for i in range(len(tr['in_index']))]
            pmid.extend([tr['pmid']]*len(tr['in_index']))
            edges.extend(e)
            edges_pos.extend([(tr['pos'][t[0]], tr['dep'][t[1]], tr['pos'][t[2]]) for t in e])
            edges_tok.extend([(tr['tokens'][t[0]], tr['dep'][t[1]], tr['tokens'][t[2]]) for t in e])
            edges_lem.extend([(tr['lemmata'][t[0]], tr['dep'][t[1]], tr['lemmata'][t[2]]) for t in e])    
    return pd.DataFrame(data={'pmid': pmid, 'edges':edges,'edges_pos': edges_pos, 'edges_tok':  edges_tok, 'edges_lem': edges_lem})

def TwoEdges(dfEdges):
    """ Connects two edges. Types: CA: common ancestor B <- A -> C, L2: 2 levels A -> B -> C """
    pmid = []
    two_edges = []
    two_edges_pos = []
    two_edges_tok = []
    two_edges_lem = []
    two_edges_type = []
    r = 0
    pmid_list = list(set(dfEdges.pmid))
    p = len(pmid_list)
    N = dfEdges.shape[0]
    L = round(N*(N/p-1))
    tic = time.perf_counter()
    for p in pmid_list:
        E = dfEdges[dfEdges.pmid == p]
        for i in range(len(E.edges)):
            # Exclude root self-edge
            if E.edges.iloc[i][0]!=E.edges.iloc[i][2]:
                for j in range(i+1,len(E.edges)):
                    if E.edges.iloc[j][0]!=E.edges.iloc[j][2]:
                        # same level
                        a11 = E.edges.iloc[i][-1]==E.edges.iloc[j][-1]
                        n11 = E.edges.iloc[i][0]==E.edges.iloc[j][0]
                        # i one level above j
                        a12 = E.edges.iloc[i][-1]==E.edges.iloc[j][-1]+1
                        n12 = E.edges.iloc[i][2]==E.edges.iloc[j][0]
                        # i one level below j
                        a21 = E.edges.iloc[i][-1]==E.edges.iloc[j][-1]-1
                        n21 = E.edges.iloc[i][0]==E.edges.iloc[j][2]
                        # Same level two nodes outgoing? (CA) or one level above other (L2)
                        if (a11 and n11) or (a12 and n12) or (a21 and n21):
                            two_edges.append((E.edges.iloc[i],E.edges.iloc[j]))
                            two_edges_pos.append((E.edges_pos.iloc[i], E.edges_pos.iloc[j]))
                            two_edges_tok.append((E.edges_tok.iloc[i], E.edges_tok.iloc[j]))
                            two_edges_lem.append((E.edges_lem.iloc[i], E.edges_lem.iloc[j]))
                            if (a11 and n11):
                                two_edges_type.append("CA")
                            else:
                                two_edges_type.append("L2")
                            pmid.append(p)
                        if r<100 or r%1000==0:
                            toc = time.perf_counter()
                            print(f"{r} ({len(two_edges)} two_edges) of {L} (estimated {round(r/L*100)}%) after {round(toc - tic):5} seconds.", end=' ')
                        r+=1
    print("\n")
    # To DataFrame
    # return pmid, two_edges_type, two_edges, two_edges_pos, two_edges_tok, two_edges_lem
    return pd.DataFrame(data={'pmid': pmid, 'two_edges_type': two_edges_type, 'two_edges':two_edges,'two_edges_pos': two_edges_pos, 'two_edges_tok':  two_edges_tok, 'two_edges_lem': two_edges_lem})

def AddLevels(tree):
    "Add levels to 'tree' variable"
    for i in range(len(tree)):
        tree[i]['levels']=TreeLevels(tree[i]['in_index'],tree[i]['out_index'])
    return tree
        
def PlotHistHtml(df,x,y,fname = 'hist.html', n_min=1):
    """ Plotly bar histogram saved to html file and displayed in browser """
    count_df = df[y].value_counts().reset_index()
    s = [str(ind) for ind in list(count_df[x])]
    count_df['index']=s
    fig = px.bar(count_df[count_df[y]>=n_min], x=x, y=y)
    url = py.offline.plot(fig, filename=fname, auto_open=True)
    return count_df, fig, url

def PlotHist(df,x,y, n_min=1, col = '#ffffff'):
    """ Plotly bar histogram with background color """
    count_df = df[y].value_counts().reset_index()
    s = [str(ind) for ind in list(count_df[x])]
    count_df['index']=s
    fig = px.bar(count_df[count_df[y]>=n_min], x=x, y=y)
    fig.update_xaxes(
        tickangle = 45,
        title_text = "",
        title_font = {"size": 20},
        title_standoff = 25,
        automargin = True
    )
    fig.update_layout(
        autosize=True,
        # width=500,
        height=800,
        margin=dict(
            l=50,
            r=50,
            b=100,
            t=100,
            pad=4
        ),
        paper_bgcolor=col,
    )    
    return count_df, fig

def DashTabsFigs(fig_list, label_list, title='', debug=True):
    app = dash.Dash(__name__)
    Li = [dcc.Tab(label=label_list[i], children=[dcc.Graph(id="graph_"+str(i), figure=fig_list[i])]) for i in range(len(fig_list))]
    if len(title):
        app.layout =html.Div(children=[html.H1(title),html.Div(dcc.Tabs(Li))])
    else:
        app.layout = html.Div(dcc.Tabs(Li))
    app.run_server(debug=debug)

def edges_two_edges_hist(tree, one_edge_col='#ffffff',two_edge_col='#ffffff'):
    # Reading input file
    # if len(sys.argv[0]):
        # print(f"Reading input file {sys.argv[0]} ... ", end = " ")
        # exec(open(sys.argv[0]).read())
        # print("done.")
    
    # Add level information to tree
    tree = AddLevels(tree)
    
    # Calculate edge configurations
    # Edges
    dfEdges = Edges(tree)
    # Two Edges configurations
    dfTwoEdges = TwoEdges(dfEdges)

    # Histograms
    # POS
    pos,fpos = PlotHist(dfEdges,'index','edges_pos', n_min=1, col = one_edge_col)
    # pos,fpos,urlpos = PlotHistHtml(dfEdges,'index','edges_pos',fname = 'example_edges_pos.html', n_min=5)
    # Tokens
    tok,ftok = PlotHist(dfEdges,'index','edges_tok', n_min=1, col = one_edge_col)
    # tok,ftok,urltok = PlotHistHtml(dfEdges,'index','edges_tok',fname = 'example_edges_tok.html', n_min=5)
    # Lemmata
    lem, flem = PlotHist(dfEdges,'index','edges_lem', n_min=1, col = one_edge_col)
    # lem, flem, urllem = PlotHistHtml(dfEdges,'index','edges_lem',fname = 'example_edges_lem.html', n_min=5)
    # CA type two edges
    ca,fca= PlotHist(dfTwoEdges[dfTwoEdges.two_edges_type=='CA'],'index','two_edges_pos', n_min=1, col = two_edge_col)
    # ca,fca,uca = PlotHistHtml(dfTwoEdges[dfTwoEdges.two_edges_type=='CA'],'index','two_edges_pos',fname = "example_CA_two_edges.html", n_min=1)
    # L2 type two edges
    l2,fl2= PlotHist(dfTwoEdges[dfTwoEdges.two_edges_type=='L2'],'index','two_edges_pos', n_min=1, col = two_edge_col)
    # l2,fl2,ul2 = PlotHistHtml(dfTwoEdges[dfTwoEdges.two_edges_type=='L2'],'index','two_edges_pos',fname = "example_L2_two_edges.html", n_min=1)

    figlist = [fpos,ftok,flem,fca,fl2]
    figtitlelist = ['Edges POS','Edges Tokens','Edges Lemmata','Two-Edges POS, common ancestor (CA)','Two-Edges POS, two-levels (L2)']
    return figlist, figtitlelist
    
def ReadJsonFile(treefile):
    # Open json tree file:
    with open(treefile,'r') as f:
        print(f"Reading tree json file {treefile} ...", end=" ")
        tree = json.load(f)
        print("done.")
    return tree

def main(fname):
    if len(fname)>3 and fname[-5:].lower()=='.json':
        tree = ReadJsonFile(fname)
    else:
        from SentencesDepTreeJson import TextFileToDepTree, LoadSpacyModel
        if 'nlp' not in dir():
            nlp = LoadSpacyModel()
        tree = TextFileToDepTree(fname)
    figlist, figtitlelist = edges_two_edges_hist(tree, one_edge_col='#E5FDFF', two_edge_col='#E5E5FF')
    DashTabsFigs(figlist, figtitlelist,title='Sentence tree edge distributions', debug=False)    
    
if __name__ == "__main__":
    if hasattr(sys, 'ps1'):
        fname = input("Please enter the filename of the text file you want analyzed [Example_Sentences.txt]: ")
        if not len(fname):
            fname = 'Example_Sentences.txt'
    else:
        fname = sys.argv[1]
    main(fname)

