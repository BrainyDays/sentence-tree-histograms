import sys
import spacy
import json

def LoadSpacyModel(model = "en_core_web_sm"):
    """ Load spacy model """
    print(f"Loading spacy model {model} ...", end=" ")
    nlp = spacy.load(model)
    print("done.")
    return nlp

def ReadTextFile(text_fname, verbose = True):
    with open(text_fname,'r') as f:
        if verbose:
            print(f"Reading text from file {text_fname} ... ", end = "")
        text = f.read()
        if verbose:
            print(f"done.")
        return text

def DepTree(sentences, pmid='', nlp=LoadSpacyModel()):
    li = []
    i = 0
    pm = len(pmid)
    for s in sentences:
        di = dict()
        d = nlp(s)
        if pm:
            di['pmid']=pmid[i]
        else:
            di['pmid']=i
        i+=1
        djo = d.to_json()
        di['text'] = djo['text']
        di['tokens'] = []
        di['lemmata'] = []
        for t in d:
            di['tokens'].append(str(t))
            di['lemmata'].append(t.lemma_)
        di['pos']=[]
        di['tag']=[]
        di['out_index']=[]
        di['in_index']=[]
        di['dep']=[]
        r = 0
        for t in djo['tokens']:
            di['pos'].append(t['pos'])
            di['tag'].append(t['tag'])
            di['in_index'].append(r)
            r+=1
            di['out_index'].append(t['head'])
            di['dep'].append(t['dep'])
        li.append(di)
    return li

def TextToSentenceList(text,splitby = '. ',endwith='.'):
    """Sentence list from text. Rule based. By default, sentences are assumed to be separated by DOT SPACE. Other delimiters can be chosen by users."""
    sentence_list = text.split(splitby)
    sentence_list[:-1] = [s + endwith for s in sentence_list[:-1]]
    return sentence_list

def TextFileToDepTree(fname, model = "en_core_web_sm"):
    # Load spacy model
    nlp = spacy.load(model)
    # Read text file
    text = ReadTextFile(fname)
    # Extract sentences
    sentence_list = TextToSentenceList(text)
    # Calculate and return dependency tree
    tree = DepTree(sentence_list, pmid='', nlp=nlp)
    return tree

if __name__ == "__main__":
    if 'nlp' not in dir():
        nlp = LoadSpacyModel()
    if hasattr(sys, 'ps1'):
        fname = input("Please enter the filename of the text file you want analyzed [Example_Sentences.txt]: ")
        if not len(fname):
            fname = 'Example_Sentences.txt'
        tree = TextFileToDepTree(fname)
    else:
        tree = TextFileToDepTree(argv[0])
        if len(argv[0]>3) and argv[0][-4:]=='.txt':
            json_fname = argv[0][-4:] + '_DepTree.json'
        else:
            json_fname = argv[0]+'_DepTree.json'
        with open(json_fname,'w') as f:
            print("Writing dependence tree to file {json_fname} ...", end = ' ')
            json.dump(tree,f)
            print("done.")

        
