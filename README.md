**Sentence Tree Histograms**

Creates histograms of edges and pairs of edges in dependence trees of sentences.
Dependence trees are calculated using [Spacy](https://spacy.io/).

**Usage:**

From command line:


python TreeDistributionStandalone.py Example_Sentences.txt 


Where Example_Sentences.txt is a text file containing text. Replace with the file name of your text file.


In a python IDE:


exec(open('TreeDistributionStandalone.py').read())


**Output:**


Histograms in a local server (open in browser)


Tab 1: **Edges POS**

Histogram of edges between parts-of-speech (POS). For abbreviations see [Spacy - Linguistic features](https://spacy.io/usage/linguistic-features)


Tab 2: **Edges Tokens**

Histogram of edges between tokens (words) in the sentences.


Tab 3: **Edges Lemmata**

Histogram of edges between [lemmata](https://spacy.io/usage/linguistic-features#lemmatization) in the sentences.


Tab 4: **Two-Edges POS, common ancestor (CA)**

Histogram of two edges patterns between POS that have the pattern of a common ancestor (CA).


Tab 5: **Two-Edges POS, two levels (L2)**

Histogram of two edges patterns between POS that have the pattern of edges occuring at two different levels (L2).


